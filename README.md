#RAPPEL DU PROJET
Objectif : 
il s'agit de créer une API avec les deux endpoints suivants :
GET /api/member/ récupère la liste de tous les adhérents au format JSON (id / nom / prénom)
GET /api/member/<ID> récupère les données d'un adhérent au format JSON à partir de l'ID fourni (id / nom / prénom / téléphone)
Contraintes :
Les données sont fournies via le fichier members.csv ci-joint
Utilisation de Symfony
Totale liberté d'utiliser les composants du framework ou des librairies ou bundles tiers
Un fichier readme doit donner les directives pour installer et faire tourner le projet

#PRE-REQUIS
Php 7.1

#INSTALLATION DU PROJET
1- Sur votre poste, créer un projet.
2- A la racine du projet taper la commande: git clone https://grapinem@bitbucket.org/grapinem/kosmos-api.git
3- Se déplacer ensuite dans kosmos-api et taper la commande composer install
4- Démarrer le serveur local avec la commande : symfony server:start
5- Pour accéder à la liste des membres, taper l'url localhost:8000/api/member
6- Pour accéder à un membre, taper l'url localhost:8000/api/member/<ID>




