<?php


namespace App;


use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CsvService
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container =$container;
    }

    public function findFile(){
    $objReader = IOFactory::createReader('Csv');
    $objReader->setInputEncoding('ISO-8859-1');
    return $objReader->load($this->container->getParameter('kernel.project_dir').'\members.csv');
    }

    public function readDataFile($spreadsheet,$id = null)
    {
        $worksheet = $spreadsheet->getActiveSheet();
        $data = [];
        $array = [
            'id',
            'nom',
            'prenom',
            'phone',
        ];
        //Si api/member/id
        if ($id) {
            // Vérification si identifiant existe
            $row=$this->findExistingRow($worksheet,$id);

            if($row){
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false);
                $index = 0;
                /** @var Cell $cell */
                foreach ($cellIterator as $key => $cell) {
                    if ($index < 5) {
                        $data[$array[$index]] = str_replace("\u{00a0}", '', $cell->getValue());
                        $index++;
                    }
                }
            }
            else {
                //Sinon message d'erreur
                $data = "aucunes données existantes pour l'identifiant: " . $id;
            }

        } else {
            // si api/member
            foreach ($worksheet->getRowIterator() as $row) {
                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(false);
                if ($row->getRowIndex() > 1) { // Skip l'entete
                    $index = 0;
                    /** @var Cell $cell */
                    foreach ($cellIterator as $key => $cell) {
                        if ($index < 5) {
                            $data[$row->getRowIndex()][$array[$index]] =str_replace("\u{00a0}", '',$cell->getValue());
                            $index++;
                        }
                    }
                }
            }
        }
        return $data;
    }

    public function findExistingRow($worksheet,$id){
        foreach ($worksheet->getRowIterator() as $row) {
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false);

            if ($row->getRowIndex() > 1) { // Skip l'entete
                $index = 0;
                foreach ($cellIterator as $key => $cell) {
                    if ($index === 0 && $cell->getValue() == $id) {
                        return $row;
                        break;
                    }
                }
            }
        }
    }
}