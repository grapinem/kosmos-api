<?php

namespace App\Controller;

use App\CsvService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends AbstractController
{

    /**
     * @Route("/api/member/{id?}", methods={"GET"}, name="api_member")
     */
    public function getMember($id,CsvService $csvService)
    {
        try{
            $file=$csvService->findFile();
            $data=$csvService->readDataFile($file,$id);

        }catch (\Exception $exception){
            $data="aucun fichier trouvé";
        }

       $response= new JsonResponse($data);
        return $response->setEncodingOptions(JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);

    }
}
